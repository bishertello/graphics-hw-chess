#include "top.h"

Chess_Board chess_board = Chess_Board();


void keyboard(){
	const float step = 1.0f;
	const float y_angle = 0.001;
	const float x_angle = 0.0005;
	if(keys['W']){
		camera.moveForward(step);
	}
	if(keys['S']){
		camera.moveForward(-step);
	}
	if(keys['A']){
		camera.moveRight(-step);
	}
	if(keys['D']){
		camera.moveRight(step);
	}
	if(keys['R']){
		camera.moveUp(step);
	}
	if(keys['F']){
		camera.moveUp(-step);
	}
	if(keys['Q']){
		camera.rotateY(y_angle);
	}
	if(keys['E']){
		camera.rotateY(-y_angle);
	}
	if(keys['Z']){
		camera.rotateX(-x_angle);
	}
	if(keys['C']){
		camera.rotateX(x_angle);
	}
}

void keyboard_white(){
	const float step = 1.0f;
	const float y_angle = 0.001;
	const float x_angle = 0.0005;
	if(keys['W']){
		white_cam.moveForward(step);
	}
	if(keys['S']){
		white_cam.moveForward(-step);
	}
	if(keys['A']){
		white_cam.moveRight(-step);
	}
	if(keys['D']){
		white_cam.moveRight(step);
	}
	if(keys['R']){
		white_cam.moveUp(step);
	}
	if(keys['F']){
		white_cam.moveUp(-step);
	}
	if(keys['Q']){
		white_cam.rotateY(y_angle);
	}
	if(keys['E']){
		white_cam.rotateY(-y_angle);
	}
	if(keys['Z']){
		white_cam.rotateX(-x_angle);
	}
	if(keys['C']){
		white_cam.rotateX(x_angle);
	}
}

int  x  = 0;



int DrawGLScene(GLvoid)	// Here's Where We Do All The Drawing
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//skybox();
	
	keyboard_white();
	//white_cam.look();
	white_cam.look();
	
	//world.draw_texturedS(textures);
	//camera.rotate(Vector3(1.0f, 1.0f, 0.0f), 90.0f);
	
	//if(x == 1) x = 2, glRotated(90, 1, 0, 0);
	//Color::show(WHITE);
	chess_board.game();
	chess_board.draw();
		
	
	frames++;
	return 1;
}


#include "bottom.h"